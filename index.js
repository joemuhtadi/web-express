const express   = require('express')
const app       = express()

app.get('/', function(request, response){
    response.send('Hello World!')
})

app.get('/about', function(request, response){
    response.send('About')
})

app.listen(3000, function(){
    let nama        = "Jundi Muhtadi"
    let role        = "Software Engineering"
    let age         = "27"
    const status    = "false"

    console.log('Nama : ' + nama)
    console.log('Role : ' + role)
    console.log('Age  : ' + age)
    console.log('Student Status : ' + status)
})